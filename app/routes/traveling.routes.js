module.exports = (app) => {
  const travelings = require('../controllers/traveling.controller');
  const router = require('express').Router();
  router.post('/', travelings.findAll);
  router.post('/create', travelings.create);
  router.put('/:id', travelings.update);
  router.delete('/:id', travelings.delete);
  app.use('/travelings', router);
};
