const moment = require('moment');
const { Op } = require('sequelize');

const convertRequestBodyToList = ({
  country,
  city,
  arrivalRange,
  departureRange,
}) => {
  return [
    { key: 'country', value: country },
    { key: 'city', value: city },
    { key: 'arrival', value: arrivalRange },
    { key: 'departure', value: departureRange },
  ];
};

const addDayToDate = (date) => {
  return moment(date).add(1, 'day').format('YYYY-MM-DD');
};

exports.getOrder = ({ sortBy, sortOrder }) => ({
  orderBy: sortBy ? sortBy : 'arrival',
  order: sortOrder ? sortOrder : 'ASC',
});

exports.getPagination = ({ currentPageNumber, rowsPerPage }) => {
  const limit = rowsPerPage ? rowsPerPage : 5;
  const offset = currentPageNumber ? currentPageNumber * limit : 0;
  return { limit: Number(limit), offset: Number(offset) };
};

exports.getPagingData = ({ data, currentPageNumber, limit }) => {
  const { count: totalItems, rows: travelings } = data;

  const currentPage = currentPageNumber ? +currentPageNumber : 0;
  const totalPages = Math.ceil(totalItems / limit);

  return { totalItems, travelings, totalPages, currentPage };
};

exports.reduceFilterMap = ({ country, city, arrivalRange, departureRange }) => {
  const requestItemList = convertRequestBodyToList({
    country,
    city,
    arrivalRange,
    departureRange,
  });

  return requestItemList.reduce((acc, { key, value }) => {
    if (typeof value === 'string' && value) {
      acc[key] = { [Op.like]: `%${value}%` };
    } else if (typeof value === 'object' && value.startDate && value.endDate) {
      acc[key] = {
        [Op.between]: [value.startDate, addDayToDate(value.endDate)],
      };
    }
    return acc;
  }, {});
};
