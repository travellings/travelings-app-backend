const { Sequelize, DataTypes } = require('sequelize');
const sequlize = require('../config/database.config');

const Traveling = sequlize.define('traveling', {
  id: {
    type: DataTypes.UUID,
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
  },
  country: {
    type: DataTypes.STRING,
  },
  city: {
    type: DataTypes.STRING,
  },
  arrival: {
    type: DataTypes.DATE,
  },
  departure: {
    type: DataTypes.DATE,
  },
});

module.exports = Traveling;
