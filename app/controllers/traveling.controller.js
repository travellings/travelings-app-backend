const Traveling = require('../models/traveling.model');
const helpers = require('../utils/helpers');

exports.create = async (req, res) => {
  const traveling = {
    country: req.body.country,
    city: req.body.city,
    arrival: req.body.arrival,
    departure: req.body.departure,
  };

  try {
    const createdTraveling = await Traveling.create(traveling);
    res.send(createdTraveling);
  } catch (error) {
    res.send({ message: error });
  }
};

exports.findAll = async (req, res) => {
  try {
    const {
      sortBy,
      sortOrder,
      currentPageNumber,
      rowsPerPage,
      country,
      city,
      arrivalRange,
      departureRange,
    } = req.body;

    const { limit, offset } = helpers.getPagination({
      currentPageNumber,
      rowsPerPage,
    });
    const { orderBy, order } = helpers.getOrder({ sortBy, sortOrder });

    const filterMap = helpers.reduceFilterMap({
      country,
      city,
      arrivalRange,
      departureRange,
    });

    const travelings = await Traveling.findAndCountAll({
      where: filterMap,
      order: [[orderBy, order]],
      limit,
      offset,
    });

    if (travelings.count > 0 && travelings.rows.length === 0) {
      const travelings = await Traveling.findAndCountAll({
        where: filterMap,
        order: [[orderBy, order]],
        limit,
        offset: 0,
      });

      const response = helpers.getPagingData({
        data: travelings,
        currentPageNumber,
        limit,
      });

      res.send(response);
    } else {
      const response = helpers.getPagingData({
        data: travelings,
        currentPageNumber,
        limit,
      });

      res.send(response);
    }
  } catch (error) {
    res.send({ message: error });
  }
};

exports.update = async (req, res) => {
  try {
    const id = req.params.id;
    const updatedTraveling = await Traveling.update(req.body, {
      where: { id: id },
    });

    if (+updatedTraveling === 1) {
      res.send({ id, ...req.body });
    } else {
      res.send({
        message: `Cannot update Traveling with id=${id}. Maybe Traveling was not found or req.body is empty!`,
      });
    }
  } catch (error) {
    res.send({ message: error });
  }
};

exports.delete = async (req, res) => {
  try {
    const id = req.params.id;

    const deletedTraveling = await Traveling.destroy({ where: { id: id } });

    if (+deletedTraveling === 1) {
      res.send({ id });
    } else {
      res.send({
        message: `Cannot delete Traveling with id=${id}. Maybe Traveling was not found~`,
      });
    }
  } catch (error) {
    res
      .status(500)
      .send({ message: `Could not delete Traveling with id=${id}` });
  }
};
