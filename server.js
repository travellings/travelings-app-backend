const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require('./app/routes/traveling.routes')(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log('server is runnig'));
